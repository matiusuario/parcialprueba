package parcialPrueba;

public class DomicilioParticular extends Propiedad {
	
	final int PORCENTAJE = 5; 
	private int habitaciones;
	private boolean usoComercialPermitido;
	
	
	
	public int getHabitaciones() {
		return habitaciones;
	}
	public void setHabitaciones(int habitaciones) {
		this.habitaciones = habitaciones;
	}
	public boolean isUsoComercialPermitido() {
		return usoComercialPermitido;
	}
	public void setUsoComercialPermitido(boolean usoComercialPermitido) {
		this.usoComercialPermitido = usoComercialPermitido;
	}
	
	@Override
	public double getAlquilerMensual() {
		return (super.getAlquilerMensual() + PORCENTAJE/100 * super.getAlquilerMensual());
	}
}
