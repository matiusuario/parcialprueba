package parcialPrueba;

public abstract class Propiedad {
	
	private int dni, m2;
	private double importeBase;
	private String direccion;
	
	public double getAlquilerMensual() {
		return this.getImporteBase();
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public int getM2() {
		return m2;
	}

	public void setM2(int m2) {
		this.m2 = m2;
	}

	public double getImporteBase() {
		return importeBase;
	}

	public void setImporteBase(double importeBase) {
		this.importeBase = importeBase;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}
