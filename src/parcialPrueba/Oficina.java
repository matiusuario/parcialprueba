package parcialPrueba;

import java.util.ArrayList;

public class Oficina extends Propiedad {
	
	private boolean pisoTecnico;
	
	private ArrayList<Servicio> servicios = new ArrayList<>(); 

	
	public boolean isPisoTecnico() {
		return pisoTecnico;
	}

	public void setPisoTecnico(boolean pisoTecnico) {
		this.pisoTecnico = pisoTecnico;
	}
	
	@Override
	public double getAlquilerMensual() {
		double acumulado = 0;
		
		for (Servicio servicio : servicios) {
			acumulado += servicio.getCostoMensual();
		}
		return (super.getAlquilerMensual() + acumulado);
	}
}
