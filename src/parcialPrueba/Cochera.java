package parcialPrueba;

public class Cochera extends Propiedad {
	
	private int capacidadVehiculos;
	private double cargoExtraPorVehiculo;
	
	
	public int getCapacidadVehiculos() {
		return capacidadVehiculos;
	}
	public void setCapacidadVehiculos(int capacidadVehiculos) {
		this.capacidadVehiculos = capacidadVehiculos;
	}
	public double getCargoExtraPorVehiculo() {
		return cargoExtraPorVehiculo;
	}
	public void setCargoExtraPorVehiculo(double cargoExtraPorVehiculo) {
		this.cargoExtraPorVehiculo = cargoExtraPorVehiculo;
	}
	
	@Override
	public double getAlquilerMensual() {
		return (super.getAlquilerMensual() + this.getCargoExtraPorVehiculo() * this.getCapacidadVehiculos());
	}
}
